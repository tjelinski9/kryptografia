# author: Tomasz Jeliński
import numpy as np
from PIL import Image
import hashlib
import random

i = 255
plain = Image.open("plain.bmp").convert('RGB')
data = np.array(plain)
img = Image.new('RGB', (plain.size[0], plain.size[1]), "black")  # new black image
pixels = img.load()  # pixel map
block_size_x = 4
block_size_y = 3


def multiplyHash(hashText):
    n = 0
    for i in range(len(hashText)):
        n += ord(hashText[i])*37
    return n % 256, (n * 47) % 256, (n * 19) % 256


for i in range(plain.size[0]//block_size_x):
    for j in range(plain.size[1]//block_size_y):
        block = []
        for x in range(block_size_x):
            for y in range(block_size_y):
                block.append(tuple(data[j*block_size_y+y][i*block_size_x+x][:]))

        cipher = ""
        for element in block:
            cipher += str(element[0]) + str(element[1]) + str(element[2])

        block_hash = hashlib.md5(cipher.encode('utf-8')).hexdigest()
        if block_size_y*block_size_x > 32:
            block_hash += (block_size_y * block_size_x) // 32 * block_hash

        newBlock = []
        for element in block:
            newBlock.append(multiplyHash(block_hash))
            block_hash = block_hash[:-1]

        for x in range(block_size_x):
            for y in range(block_size_y):
                pixels[i*block_size_x+x, j*block_size_y+y] = newBlock[y*block_size_x + x]

img.save("ecb_crypto.bmp")
print("successfully encrypted to \"ecb_crypto.bmp\"")

img = Image.new('RGB', (plain.size[0], plain.size[1]), "black")
pixels = img.load()
block = []
newBlock = []
for i in range(plain.size[0] // block_size_x):
    for j in range(plain.size[1] // block_size_y):
        previous_block = newBlock[:]
        if not block:  # for the first block (IV)
            for x in range(block_size_x):
                for y in range(block_size_y):
                    previous_block.append([random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)])

        block = []
        for x in range(block_size_x):
            for y in range(block_size_y):
                block.append(tuple(data[j*block_size_y+y][i*block_size_x+x][:]))

        cipher = ""
        for element in block:
            cipher += str(element[0]) + str(element[1]) + str(element[2])
        block_hash = hashlib.md5(cipher.encode('utf-8')).hexdigest()

        previous_block_cipher = ""
        for element in previous_block:
            previous_block_cipher += str(element[0]) + str(element[1]) + str(element[2])
        previous_block_hash = hashlib.md5(previous_block_cipher.encode('utf-8')).hexdigest()

        final_hash_list = [chr(ord(a) ^ ord(b)) for a, b in zip(previous_block_hash, block_hash)]
        final_hash = "".join(final_hash_list)
        if block_size_y*block_size_x > 32:
            final_hash += (block_size_y*block_size_x) // 32 * final_hash

        newBlock = []
        for element in block:
            newBlock.append(multiplyHash(final_hash))
            final_hash = final_hash[:-1]
        for x in range(block_size_x):
            for y in range(block_size_y):
                pixels[i*block_size_x+x, j*block_size_y+y] = newBlock[y*block_size_x + x]

img.save("cbc_crypto.bmp")
print("successfully encrypted to \"cbc_crypto.bmp\"")
