#pisać kto jest autorem programu

import sys

global mode
# mode = 'a'

alphabet = {}
for l in range(26):
    alphabet[l] = chr(ord('a') + l)
# print(alphabet)


def GCD(x, y):
    while y:
        x, y = y, x % y

    return x


def modeOfArray(arr):
    m = max([arr.count(a) for a in arr])
    return [x for x in arr if arr.count(x) == m][0] if m > 1 else None


def get_key(val):
    for key, value in alphabet.items():
        if val == value:
            return key


def calculatePositions(text):
    numbers = []
    for n in text:
        if n != ' ':
            numbers.append(get_key(n))
        else:
            numbers.append(' ')
    return numbers


def calculateLetters(numbers):
    letters = []
    for n in numbers:
        if n != ' ':
            letters.append(alphabet[n])
        else:
            letters.append(' ')
    return letters


def findKeyAfinite(n1, n2, m1, m2):  # four values to calculate key with a piece of plain text (n*y)+x=m mod 26
    m = (m1 - m2) % 26
    i = 0
    while (m + i * 26) % (n1 - n2) != 0:
        i += 1
        if i > 1000:
            break

    else:
        y = (m + i * 26) / (n1 - n2)
        return [int(y) % 26, int((m1 - n1 * y) % 26)]

    return None


def modInverse(a, m):
    for x in range(1, m):
        if ((a % m) * (x % m)) % m == 1:
            return x
    print("This key cannot be modulo inversed in this alphabet")
    print("The program will stop working")
    exit()


def checkForCapitals(tempText):
    capitals = []
    text = ''
    for l in range(len(tempText)):
        if tempText[l] == ' ':
            text += ' '
            continue
        if ord(tempText[l]) <= 90:
            capitals.append(l)
            text += chr(ord(tempText[l]) + 32)
        else:
            text += tempText[l]

    return [text, capitals]


def capitalizeSpecific(text, capitals):
    newText = ''
    caps = capitals[:]
    if caps:
        for i in range(len(text)):
            if caps:
                if i == caps[0]:
                    newText += chr(ord(text[i]) - 32)
                    del caps[0]
                else:
                    newText += text[i]
            else:
                newText += text[i]
        return newText
    else:
        return text


def cipher():
    try:
        with open('plain.txt', 'r') as file:
            for line in file:
                tempText = line
    except:
        print('no plain text file')
        return

    try:
        with open('key.txt', 'r') as file:
            for line in file:
                key = line
    except:
        print('no key file')
        return

    (text, capitals) = checkForCapitals(tempText)

    if mode == 'c':
        key = int(key.split()[0])

        numbers = calculatePositions(text)
        numbers = [(i + key) % 26 if i != ' ' else ' ' for i in numbers]

        outFile = open("crypto.txt", "w")
        outFile.write(capitalizeSpecific(''.join(calculateLetters(numbers)), capitals))
        outFile.close()

    elif mode == 'a':
        key, factor = int(key.split()[0]), int(key.split()[1])

        numbers = calculatePositions(text)
        numbers = [(i * factor + key) % 26 if i != ' ' else ' ' for i in numbers]

        outFile = open("crypto.txt", "w")
        outFile.write(capitalizeSpecific(''.join(calculateLetters(numbers)), capitals))
        outFile.close()


def decipher():
    try:
        with open('crypto.txt', 'r') as file:
            for line in file:
                tempText = line
    except:
        print('no crypto file')
        return

    try:
        with open('key.txt', 'r') as file:
            for line in file:
                key = line
    except:
        print('no crypto file')
        return

    (text, capitals) = checkForCapitals(tempText)

    if mode == 'c':
        key = int(key.split()[0])

        numbers = calculatePositions(text)
        numbers = [(i - key) % 26 if i != ' ' else ' ' for i in numbers]

        outFile = open("decrypt.txt", "w")
        outFile.write(capitalizeSpecific(''.join(calculateLetters(numbers)), capitals))
        outFile.close()

    elif mode == 'a':
        key, factor = int(key.split()[0]), int(key.split()[1])

        numbers = calculatePositions(text)
        numbers = [(modInverse(factor, 26) * (i - key)) % 26 if i != ' ' else ' ' for i in numbers]

        outFile = open("decrypt.txt", "w")
        outFile.write(capitalizeSpecific(''.join(calculateLetters(numbers)), capitals))
        outFile.close()


def cryptoanalysis():
    with open('crypto.txt', 'r') as file:
        for line in file:
            tempText = line

    with open('extra.txt', 'r') as file:
        for line in file:
            tempPlain = line

    (text, capitals) = checkForCapitals(tempText)
    (plain, capitals) = checkForCapitals(tempPlain)
    plainLength = len(plain)

    if mode == 'c':
        key = 26 - (calculatePositions(plain[0])[0] - calculatePositions(text[0])[0]) % 26

        numbers = [(i - key) % 26 if i != ' ' else ' ' for i in calculatePositions(text[plainLength:len(text)])]
        outFile = open("decrypt.txt", "w")
        outFile.write(capitalizeSpecific(plain + ''.join(calculateLetters(numbers)), capitals))
        outFile.close()
        outFile = open("key-found.txt", "w")
        outFile.write(str(key))
        outFile.close()

    if mode == 'a':
        j = 1
        keys = []
        for i in range(plainLength - 2):
            if i > 10:
                break
            if plain[i] == ' ':
                j -= 1
                continue
            if plain[i + j] == ' ':
                j += 1

            keys.append(findKeyAfinite(calculatePositions(plain[i])[0], calculatePositions(plain[i + j])[0],
                                       calculatePositions(text[i])[0], calculatePositions(text[i + j])[0]))

        # print(keys)
        key = modeOfArray(keys)
        # print(key)

        numbers = [(modInverse(key[0], 26) * (i - key[1])) % 26 if i != ' ' else ' ' for i in
                   calculatePositions(text[plainLength:len(text)])]

        outFile = open("decrypt.txt", "w")
        outFile.write(capitalizeSpecific(plain + ''.join(calculateLetters(numbers)), capitals))
        outFile.close()
        outFile = open("key-found.txt", "w")
        outFile.write(str(key[1]) + ' ' + str(key[0]))
        outFile.close()


def bruteForce():
    with open('crypto.txt', 'r') as file:
        for line in file:
            tempText = line

    (text, capitals) = checkForCapitals(tempText)
    numbers = calculatePositions(text)

    if mode == 'c':
        outFile = open("decrypt.txt", "w")
        for key in range(1, 26):
            outFile.write(
                capitalizeSpecific(''.join(calculateLetters([(i + key) % 26 if i != ' ' else ' ' for i in numbers])),
                                   capitals) + '\n')
        outFile.close()

    if mode == 'a':
        outFile = open("decrypt.txt", "w")
        for factor in range(1, 26):
            if GCD(factor, 26) == 1:
                for key in range(26):
                    outFile.write(capitalizeSpecific(''.join(
                        calculateLetters([(i * factor + key) % 26 if i != ' ' else ' ' for i in numbers])),
                        capitals) + '\n')
        outFile.close()


if sys.argv[1] == '-c':
    mode = 'c'
elif sys.argv[1] == '-a':
    mode = 'a'
else:
    print('unrecognized algorithm, choose from: -c, -a')

if sys.argv[2] == '-e':
    cipher()
elif sys.argv[2] == '-d':
    decipher()
elif sys.argv[2] == '-j':
    cryptoanalysis()
elif sys.argv[2] == '-k':
    bruteForce()
else:
    print('unrecognized option, choose from: -e, -d, -j, -k')

# cipher()
# decipher()
# cryptoanalysis()
# bruteForce()
