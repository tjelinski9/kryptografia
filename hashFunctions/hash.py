# author: Tomasz Jeliński
import hashlib
import sys
hash_methods = ["md5", "sha1", "sha224", "sha256", "sha384", "sha512", "b2"]

name = open(r"personal.txt", encoding="utf-8").read()
name_ = open(r"personal_.txt", encoding="utf-8").read()
# print(name)
original_stdout = sys.stdout
with open('hashes.txt', 'w') as f:
    sys.stdout = f

    hash = hashlib.md5(name.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.md5(name_.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha1(name.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha1(name_.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha224(name.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha224(name_.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha256(name.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha256(name_.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha384(name.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha384(name_.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha512(name.encode('utf-8')).hexdigest()
    print(hash)
    hash = hashlib.sha512(name_.encode('utf-8')).hexdigest()
    print(hash)


def countDiff(binary1, binary2):
    b1 = [0, 0, 0, 0]
    b2 = b1[:]
    count = 0
    for i in range(len(binary1)-2):
        b1[-1-i] = binary1[-1-i]
    for i in range(len(binary2) - 2):
        b2[-1 - i] = binary2[-1 - i]
    for i in range(4):
        if b1[i] != b2[i]:
            count += 1

    return count


with open('diff.txt', 'w') as f:
    sys.stdout = f

    previous_line = None
    different_bits = 0
    j = 0
    output = []
    try:
        with open('hashes.txt', 'r') as file:
            for line in file:
                j += 1
                if previous_line:
                    for i in range(len(line) - 1):
                        different_bits += countDiff(bin(int(line[i], 16)), bin(int(previous_line[i], 16)))
                    bits = (len(line) - 1) * 4
                    print(hash_methods[int(j/2)-1], end=":\n")
                    print(previous_line[:-1])
                    print(line[:-1])
                    print("different bits:", different_bits, "out of", bits, "-", different_bits / bits * 100, '%\n')
                    different_bits = 0
                    previous_line = None
                else:
                    previous_line = line[:]

    except IOError:
        print('no hashes text file')
        exit(1)

sys.stdout = original_stdout
