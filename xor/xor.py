# autor: Tomasz Jeliński
import sys


def prepare():
    numOfChars = 64
    text = ""
    try:
        with open('orig.txt', 'r') as file:
            for line in file:
                text += line
    except IOError:
        print('no orig text file')
        return

    text = text.lower()
    for l in text:
        asc = ord(l)
        if asc == 10:
            text = text[:text.find(l)] + ' ' + text[text.find(l) + 1:]
        elif asc > 122 or (asc < 97 and asc != 32):
            text = text[:text.find(l)] + text[text.find(l) + 1:]

    try:
        with open('plain.txt', 'w') as file:
            while (text):
                if len(text) > numOfChars:
                    file.writelines(text[:numOfChars] + '\n')
                    text = text[numOfChars:]
                else:
                    text = None
    except IOError:
        print('error while creating plain.txt file')
        exit()


def cipher(inFileName="plain.txt", outFileName="crypto.txt", keyFileName="key.txt"):
    text = []
    key = ''
    deciphering = False
    try:
        with open(inFileName, 'r') as file:
            for line in file:
                text.append(line)
    except IOError:
        print('no plain text file')
        return

    try:
        with open(keyFileName, 'r') as file:
            for line in file:
                key = line
    except IOError:
        print('no key file')
        return

    if len(key) != len(text[0])-1:
        print('wrong key length')

    for line in text:
        line = line[:-1]
        for l in line:
            asc = ord(l)
            if asc < 97 and asc != 32:
                deciphering = True

    outFile = open(outFileName, "w")
    for line in text:
        line = line[:-1]
        encryptedLine = ""
        for l in range(len(line)):
            if deciphering:
                encryptedLine += chr((ord(line[l]) - 33) ^ (ord(key[l])))  # to reverse avoiding vv
            else:
                encryptedLine += chr((ord(line[l]) ^ ord(key[l])) + 33)  # to avoid nonprintable
        outFile.write(encryptedLine + '\n')
        # print(encryptedLine)
    outFile.close()


def cryptoanalysis():
    text = []
    try:
        with open('crypto.txt', 'r') as file:
            for line in file:
                text.append(line)
    except IOError:
        print('no crypto text file')
        return

    key = ""
    for l in range(len(text[0]) - 1):  # dla kazdego znaku w linii krecimy po liniach
        # foundSpace = False
        space = None
        tempSpace = None
        for j in range(len(text) - 1):  # krecimy po liniach pionowo
            for i in range(j, len(text) - 1):
            # print(text[j][l])
                if ((ord(text[j][l]) - 33) ^ (ord(text[i][l]) - 33)) >> 5 == 2:
                    # znaleziono 1 spacje posrod 1 i 2
                    if ((ord(text[i][l]) - 33) ^ (ord(text[i+1][l]) - 33)) >> 5 == 2:
                        # jedna spacja posrod 2 i 3
                        if ((ord(text[j][l]) - 33) ^ (ord(text[i+1][l]) - 33)) == 0:
                            # jeden z dwoch przypadkow - ' i ' lub 'i i'
                            # oznaczenie tempSpacji w 1 i 3 ale szukamy dalej
                            tempSpace = j
                            continue
                        else:
                            # oznaczenie 2 jako spacji
                            space = i
                            break
                    elif ((ord(text[i][l]) - 33) ^ (ord(text[i+1][l]) - 33)) == 0:
                        # 2 == 3 wiec jedziemy dalej
                        continue
                    else:
                        # 2 i 3 to litery, wiec 1 to spacja
                        # oznaczenie 1 jako spacji
                        space = j
                        break
                elif ((ord(text[j][l]) - 33) ^ (ord(text[i][l]) - 33)) == 0:
                    # oba są takie same
                    continue
                else:
                    # oba znaki to litery
                    continue

        if space:
            # key += space ^ cipher with space
            key += chr(32 ^ (ord(text[space][l]) - 33))
        elif tempSpace:
            key += chr(32 ^ (ord(text[tempSpace][l]) - 33))
        else:
            key += 'X'

    outFile = open("key-found.txt", "w")
    outFile.write(key)
    outFile.close()
    cipher("crypto.txt", "decrypt.txt", "key-found.txt")
    # print('key:', key)

    text = ""
    with open("decrypt.txt", 'r') as file:
        for line in file:
            text += line[:-1]
    with open("decrypt.txt", "w") as file:
        file.write(text)


prepare()
cipher()
cryptoanalysis()

# if sys.argv[2] == '-p':
#     prepare()
# elif sys.argv[2] == '-e':
#     cipher()
# elif sys.argv[2] == '-k':
#     cryptoanalysis()
# else:
#     print('unrecognized option, choose from: -p (prepare text), -e (cipher), -k (cryptoanalysis)')
